class AddDelayedJobIdToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :delayed_job_id, :integer
  end
end
