class AddKeywordColumnToJobs < ActiveRecord::Migration
  def change
    add_column :jobs, :keyword_column, :integer, :default => 0
  end
end
