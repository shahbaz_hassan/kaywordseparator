class CreateJobs < ActiveRecord::Migration
  def change
    create_table :jobs do |t|
      t.string :job_name
      t.integer :status
      t.string :csv_input

      t.timestamps null: false
    end
  end
end
