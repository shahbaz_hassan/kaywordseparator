class Job < ActiveRecord::Base

  mount_uploader :csv_input, JobCsvUploader
  
  validates :job_name, :presence => true
  validates :csv_input, :presence => true

  enum :status => [ :pending, :processing, :completed, :terminated]

  def background_process(retries = 0)
    if self.csv_input.present?
      begin
        self.update_attributes(:status => :processing)

        ActiveRecord::Base.transaction do
          if retries >= 1
            options = {:headers => false, :encoding => 'UTF-8:UTF-8', :skip_blanks => true }
          else
            options = {:headers => false, :encoding => 'ISO-8859-1:UTF-8', :skip_blanks => true }
          end
          #options = { :headers => true, :encoding => 'iso-8859-1:utf-8', :skip_blanks => true }
          csv_text = File.read(self.csv_input.path.to_s, options)
          csv_text = csv_text.gsub /\n/, ''
          header = true
          #      output_csv = []

      

          CSV.parse(csv_text, options) do |row|
            CSV.open(Rails.root.join("public/reports/", "#{self.id}-report.csv"), "a+") do |output_csv|
              if header
                output_csv << row
                header = false
                next
              end
              next if row[0].blank? or row[0].strip.blank?
              if self.keyword_column.to_i > 0
                strip_row = row[self.keyword_column.to_i]
              else
                strip_row = row[0]
              end
              strip_row.split(',').map(&:strip).each do |keyword|
                next if keyword.blank? or keyword.strip.blank?
                tmp_array = row.dup.map { |item| item.strip.squish if item.present? }
                if self.keyword_column.to_i > 0
                  tmp_array[self.keyword_column.to_i] = keyword
                else
                  tmp_array[0] = keyword
                end
                output_csv << tmp_array
              end
            end
          end
          self.update_attributes(:status => :completed)
        end
      rescue Exception => e
        if retries >= 1
          # self.errors_while_uploading_file(@processed_rows)
          Rails.logger.debug e
          Rails.logger.debug "here it is failed"
          self.update_attributes(:status => :terminated)
          # ExceptionNotifier.notify_exception(e)
        else
          self.background_process(1)
        end

      end
    end
  end
end