require 'csv'
class JobController < ApplicationController

  def index
    @new_job = Job.new
    @jobs = Job.all.order("updated_at DESC")
  end

  def create
    @new_job = Job.new(job_params)
    options = {}
    options1 = {}
    if @new_job.save
      job = @new_job.delay.background_process
      @new_job.update_attributes(:status => :pending, :delayed_job_id => job.id)
      @newo_job = Job.new
      options = { :flash => { :success => "Job saved!" } }
      options1 = { :active_tab => 0 }
    else
      options = { :flash => { :error => @new_job.errors.full_messages.join("<br/>") } }
      options1 = { :active_tab => 1 }
    end
    redirect_to root_path(options1), options
  end

  def job_params
    params.require(:job).permit(:job_name, :csv_input, :keyword_column)
  end
  
  def export
    job = Job.find_by_id(params[:id])
    if job.present?
      send_file "#{Rails.root}/public/reports/#{job.id}-report.csv", :type=> 'text/csv'
    end
  end
  def delete
    @job = Job.find_by_id(params[:id])
    running_job = Delayed::Job.find_by_id(@job.delayed_job_id)
    if running_job.present?
      running_job.destroy
    end
    @job.destroy
    redirect_to root_path, :flash => { :success => "Job is deleted!" } and return
  end

  def stop
    @job = Job.find_by_id(params[:id])
    running_job = Delayed::Job.find_by_id(@job.delayed_job_id)

    if running_job.blank?
      @job.update_attributes(:status => :terminated )
      running_job.destroy if running_job.present?
      redirect_to root_path, :flash => { :error => "Job is not running but terminated!" } and return
    end
    running_job.destroy if running_job.present?
    @job.update_attributes(:status => :terminated )
    redirect_to root_path, :flash => { :success => "Job stopped!" } and return
  end


end
