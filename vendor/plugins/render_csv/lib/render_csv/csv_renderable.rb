require 'csv'

module RenderCsv
  module CsvRenderable
    # Converts an array to CSV formatted string
    # Options include:
    # :only => [:col1, :col2] # Specify which columns to include
    # :except => [:col1, :col2] # Specify which columns to exclude
    # :add_methods => [:method1, :method2] # Include addtional methods that aren't columns
    def to_csv(options = {})

      if options[:find_in_batch].present?
        find_in_batch_options = options[:find_in_batch]
        if find_in_batch_options[:object].blank?
          raise Exception.new("Please attach an object")
        end
        batch_object = find_in_batch_options[:object]
        batch_size = if find_in_batch_options[:batch_size].present?
          find_in_batch_options[:batch_size]
        else
          2000
        end

        if options[:column_names].present?
          header_row = options[:column_names]
        end

        columns = batch_object.to_s.classify.constantize.column_names
        columns &= options[:only].map(&:to_s) if options[:only]
        columns -= options[:except].map(&:to_s) if options[:except]
        columns += options[:add_methods].map(&:to_s) if options[:add_methods]

        CSV.generate do |row|
          if options[:column_names].present?
            row << header_row
          else
            row << columns
          end
          eval("self.#{batch_object.to_sym}.find_each(:batch_size => #{batch_size}) { |obj| row << columns.map { |c| obj.send(c) } }")
        end

      else

        return '' if empty?
        return join(',') unless first.class.respond_to? :column_names

        columns = first.class.column_names
        columns &= options[:only].map(&:to_s) if options[:only]
        columns -= options[:except].map(&:to_s) if options[:except]
        columns += options[:add_methods].map(&:to_s) if options[:add_methods]

        CSV.generate(:encoding => 'utf-8') do |row|
          row << columns
          self.each do |obj|
            row << columns.map { |c| obj.send(c) }
          end
        end
      end

    end
  end
end
